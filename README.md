# Mailchimp marketing

[Mailchimp](https://mailchimp.com/) email campaign service integration module.

## What is the difference?

As oppose to another [mailchimp](https://www.drupal.org/project/mailchimp) module 
`Mailchimp marketing` uses official php [mailchimp marketing](https://github.com/mailchimp/mailchimp-marketing-php) 
library.

## Roadmap:

* Taxonomy integration: syncing groups and taxonomy terms
* Creating RSS campaigns based on taxonomy terms
* Webform integration
