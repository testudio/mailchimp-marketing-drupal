<?php

namespace Drupal\mailchimp_marketing\Form;

use GuzzleHttp\Exception\ClientException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\mailchimp_marketing\Controller\MailchimpController;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Mailchimp marketing settings.
 */
class MailchimpMarketingSettingsForm extends ConfigFormBase {

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The mailchimp service.
   *
   * @var \Drupal\mailchimp_marketing\Controller\MailchimpController
   */
  protected $mailchimp;

  /**
   * Constructs a new NegotiationUrlForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\mailchimp_marketing\Controller\MailchimpController $mailchimp
   *   The mailchimp service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, LoggerInterface $logger, MailchimpController $mailchimp) {
    parent::__construct($config_factory);
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->mailchimp = $mailchimp;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('logger.channel.mailchimp_marketing'),
      $container->get('mailchimp_marketing.mailchimp')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailchimp_marketing_settings';
  }

  protected function getEditableConfigNames() {
    return ['mailchimp_marketing.settings'];
  }

  protected function getEditableConfig() {
    $config_names = $this->getEditableConfigNames();
    $config_name = reset($config_names);
    return $this->config($config_name);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->getEditableConfig();

    $mc_api_url = Url::fromUri('http://admin.mailchimp.com/account/api', ['attributes' => ['target' => '_blank']]);
    $form['mailchimp_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('mailchimp_api_key'),
      '#description' => $this->t('The API key for your Mailchimp account. Get or generate a valid API key at your @apilink.', ['@apilink' => Link::fromTextAndUrl($this->t('Mailchimp API Dashboard'), $mc_api_url)->toString()]),
    ];

    if ($this->mailchimp->pingSuccess()) {
      $form['mailchimp_server_prefix'] = [
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#type' => 'markup',
        '#markup' => $this->t('<strong>Server Prefix (based on API key)</strong>: @server_prefix', [
          '@server_prefix' => $config->get('mailchimp_server_prefix'),
        ]),
      ];

      $lists = [];
      $mailchimp = $this->mailchimp->getConnection();
      $response = $mailchimp->lists->getAllLists();
      foreach ($response->lists as $list) {
        $list_id = $list->id;
        $list_name = $list->name;
        $lists[$list_id] = $list_name;
      }

      $form['mailchimp_default_list'] = [
        '#type' => 'select',
        '#title' => t('Default list'),
        '#options' => $lists,
      ];

      $form['actions']['mailchimp_test'] = [
        '#weight' => 10,
        '#type' => 'submit',
        '#value' => 'Test Connection',
        '#submit' => ['::submitFormTestConnection'],
      ];

      $form['actions']['mailchimp_create_tag'] = [
        '#weight' => 20,
        '#type' => 'submit',
        '#value' => 'Create tag',
        '#submit' => ['::submitFormCreateTag'],
      ];

      $form['actions']['mailchimp_create_contact'] = [
        '#weight' => 30,
        '#type' => 'submit',
        '#value' => 'Create contact',
        '#submit' => ['::submitFormCreateContact'],
      ];

      $form['actions']['mailchimp_delete_all_tags'] = [
        '#weight' => 40,
        '#type' => 'submit',
        '#value' => 'Delete all tags',
        '#submit' => ['::submitFormDeleteAllTags'],
      ];
    }
    else {
      $this->messenger->addError($this->t('Error when connecting to mailchimp. Please check the API key.'));
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Generate prefix.
    $api_key = $form_state->getValue('mailchimp_api_key');
    $api_key_exploded = explode('-', $api_key);
    $server_prefix = end($api_key_exploded);
    $default_list = $form_state->getValue('mailchimp_default_list');

    $config = $this->getEditableConfig();
    $config
      ->set('mailchimp_api_key', $api_key)
      ->set('mailchimp_server_prefix', $server_prefix)
      ->set('mailchimp_default_list', $default_list)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Form submission handler for testing connection.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitFormTestConnection(array &$form, FormStateInterface $form_state) {
    try {
      $mailchimp = $this->mailchimp->getConnection();
      $response = print_r($mailchimp->ping->get(), TRUE);

      $this->messenger->addStatus($this->t('Mailchimp Response: <br /><pre>@response</pre>',
        ['@response' => $response])
      );
    }
    catch (ClientException $e) {
      $this->messenger->addError($this->t('Mailchimp exception: <br /><pre>@exception</pre>', [
        '@exception' => $e->getMessage(),
      ]));
    }
  }

  /**
   * Form submission handler for testing connection.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitFormCreateTag(array &$form, FormStateInterface $form_state) {
    try {
      $mailchimp = $this->mailchimp->getConnection();
      //$response = $mailchimp->lists->getAllLists();
      $list_id = $form_state->getValue('mailchimp_default_list');
      //$list_id = '7f4f516be6';
      /*$response = $mailchimp->lists->listSegments($list_id);
      if ($response) {
        foreach ($response->segments as $segment) {
          $segment_id = $segment->id;
          $segment_name = $segment->name;
          $segment_member_count = $segment->member_count;
          $segment_list_id = $segment->list_id;
        }
      }*/
      //dump($response);

      // Create tag.
      // $(echo -n "v_roudakov@yahoo.com" | md5sum | cut -f1 -d' ')
      $segment = $mailchimp->lists->createSegment($list_id, '{"name":"Segment 1","static_segment":[]}');
      // Create segment (dynamic tag).
      $segment = $mailchimp->lists->createSegment($list_id, '{"name":"Segment 2","options":{"match":"any","conditions":[]}}');
      //'{"name":"","static_segment":[],"options":{"match":"any","conditions":[]}}'

    }
    catch (ClientException $e) {
      $warning = $this->t('Error while creating tag. Details: <br /><pre>@exception</pre>', [
        '@exception' => $e->getMessage(),
      ]);
      $this->messenger->addWarning($warning);
      $this->logger->warning($warning);
    }
  }

  /**
   * Form submission handler for testing connection.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitFormDeleteAllTags(array &$form, FormStateInterface $form_state) {
    try {
      $mailchimp = $this->mailchimp->getConnection();
      $list_id = $form_state->getValue('mailchimp_default_list');
      $response = $mailchimp->lists->listSegments($list_id);
      if ($response) {
        foreach ($response->segments as $segment) {
          $segment_id = $segment->id;
          $segment_name = $segment->name;
          $segment_member_count = $segment->member_count;
          $segment_list_id = $segment->list_id;

          $mailchimp->lists->deleteSegment($segment_list_id, $segment_id);
        }
      }
    }
    catch (ClientException $e) {
      $this->logger->warning('Error while deleting tags. Details: <br /><pre>@exception</pre>',
        [
          '@exception' => $e->getMessage(),
        ]);
    }
  }

  /**
   * Form submission handler for testing connection.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitFormCreateContact(array &$form, FormStateInterface $form_state) {
    try {
      $mailchimp = $this->mailchimp->getConnection();
      $list_id = $form_state->getValue('mailchimp_default_list');
      $segments = $mailchimp->lists->listSegments($list_id);
      dump($segments);
      $segment_id = '561622';
      //"id": 561618 "name": "Woot"
      //"id": 561622 "name": "Segment 1"

      $mailchimp->lists->addListMember($list_id, '{"email_address":"j_malikova@yahoo.com","email_type":"","status":"subscribed","merge_fields":{},"interests":{},"language":"","vip":false,"location":{"latitude":0,"longitude":0},"marketing_permissions":[],"ip_signup":"","timestamp_signup":"","ip_opt":"","timestamp_opt":"","tags":[561618,561622]}');
      // Existing email.
      $mailchimp->lists->createSegmentMember($list_id, $segment_id, '{"email_address":"v_roudakov@yahoo.com"}');
      //$mailchimp->lists->createSegmentMember($list_id, $segment_id, '{"email_address":"j_malikova@yahoo.com"}');

      //die();
    }
    catch (ClientException $e) {
      $warning = $this->t('Error while creating contact. Details: <br /><pre>@exception</pre>', [
        '@exception' => $e->getMessage(),
      ]);

      $this->messenger->addWarning($warning);
      $this->logger->warning($warning);
    }
  }

}
