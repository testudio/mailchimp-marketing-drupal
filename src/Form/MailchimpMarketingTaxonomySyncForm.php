<?php

namespace Drupal\mailchimp_marketing\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\mailchimp_marketing\Controller\MailchimpController;
use GuzzleHttp\Exception\ClientException;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Mailchimp marketing settings.
 */
class MailchimpMarketingTaxonomySyncForm extends FormBase {

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The mailchimp service.
   *
   * @var \Drupal\mailchimp_marketing\Controller\MailchimpController
   */
  protected $mailchimp;

  /**
   * Constructs a new NegotiationUrlForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\mailchimp_marketing\Controller\MailchimpController $mailchimp
   *   The mailchimp service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(MessengerInterface $messenger, LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, MailchimpController $mailchimp, ModuleHandlerInterface $module_handler) {
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->entityTypeManager = $entity_type_manager;
    $this->mailchimp = $mailchimp;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('logger.channel.mailchimp_marketing'),
      $container->get('entity_type.manager'),
      $container->get('mailchimp_marketing.mailchimp'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailchimp_marketing_taxonomy_sync';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    if ($this->moduleHandler->moduleExists('taxonomy')) {
      $vocabs = $this->entityTypeManager
        ->getStorage('taxonomy_vocabulary')
        ->loadMultiple();

      if ($vocabs) {
        foreach ($vocabs as $vid => $vocab) {
          $lists[$vid] = $vocab->get('name');
        }

        $form['vocabulary_to_sync'] = [
          '#type' => 'select',
          '#title' => t('Vocabulary'),
          '#required' => TRUE,
          '#options' => $lists,
        ];

        $form['mailchimp_delete_tags'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Delete current tags'),
          '#description' => $this->t('Delete all tags that are matching the pattern <strong>Tag [id]</strong>. Warning, this will delete existing tags.'),
        ];

        $form['is_debug'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Display debug messages.'),
          '#description' => $this->t('Display debug messages when creting / deleting.'),
        ];

        $form['actions']['mailchimp_test'] = [
          '#weight' => 10,
          '#type' => 'submit',
          '#value' => 'Sync vocabulary',
        ];
      }
      else {
        $link = Url::fromRoute('entity.taxonomy_vocabulary.add_form');
        $warning = $this->t('No taxonomy vocabularies found. @link.', [
          '@link' => Link::fromTextAndUrl($this->t('Add vocabulary'), $link)->toString(),
        ]);
        $this->messenger->addWarning($warning);
      }
    }
    else {
      $link = Url::fromRoute('system.modules_list');
      $warning = $this->t('Taxonomy module is disabled. @link.', [
        '@link' => Link::fromTextAndUrl($this->t('Manage modules'), $link)->toString(),
      ]);
      $this->messenger->addWarning($warning);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->mailchimp->pingSuccess()) {
      $mailchimp = $this->mailchimp->getConnection();
      $vid = $form_state->getValue('vocabulary_to_sync');
      $delete_tags = $form_state->getValue('mailchimp_delete_tags');
      $is_debug = $form_state->getValue('is_debug');
      $list_id = $this->mailchimp->getDefaultList();

      if ($delete_tags) {
        $response = $mailchimp->lists->listSegments($list_id, NULL, NULL, '200');
        if ($response) {
          foreach ($response->segments as $segment) {
            $segment_id = $segment->id;
            $segment_name = $segment->name;
            $segment_member_count = $segment->member_count;
            $segment_list_id = $segment->list_id;

            $mailchimp->lists->deleteSegment($segment_list_id, $segment_id);
            $info = $this->t('Deleted <em>@tag</em> tag with @count members from <em>@list</em> audience list.', [
              '@tag' => $segment_name,
              '@count' => $segment_member_count,
              '@list' => $list_id,
            ]);
            $this->logger->info($info);
            if ($is_debug) {
              $this->messenger->addMessage($info);
            }
          }
        }
      }

      $terms = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->loadTree($vid);

      if ($terms) {
        foreach ($terms as $tid => $term) {
          $segment = $mailchimp->lists->createSegment($list_id, '{"name":"' . $term->name->value . ' [' . $term->id() . ']","static_segment":[]}');
          $info = $this->t('Created <em>@tag</em> static tag for <em>@list</em> audience list.', [
            '@tag' => $term->name->value,
            '@list' => $list_id,
          ]);
          $this->logger->info($info);
          if ($is_debug) {
            $this->messenger->addMessage($info);
          }
        }
      }
    }
    else {
      $link = Url::fromRoute('mailchimp_marketing.admin');
      $warning = $this->t('Cannot contact Mailchimp API. API key might be incorrect. @link.', [
        '@link' => Link::fromTextAndUrl($this->t('Manage mailchimp configuration'), $link)->toString(),
      ]);
      $this->messenger->addError($warning);
    }
  }

}
