<?php

namespace Drupal\mailchimp_marketing\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use MailchimpMarketing\ApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides route responses for coffee.module.
 */
class MailchimpController extends ControllerBase {

  /**
   * The coffee config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a new CoffeeController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('mailchimp_marketing.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Mailchimp connection getter.
   *
   * @return \MailchimpMarketing\ApiClient
   *   Mailchimp connection object.
   */
  public function getConnection() {
    $api_key = $this->config->get('mailchimp_api_key');
    $server_prefix = $this->config->get('mailchimp_server_prefix');

    // Test the connection.
    $mailchimp = new ApiClient();
    $mailchimp->setConfig([
      'apiKey' => $api_key,
      'server' => $server_prefix,
    ]);

    return $mailchimp;
  }

  /**
   * Test connection.
   *
   * @return bool
   *   True if connection successful.
   */
  public function pingSuccess() {
    try {
      $mailchimp = $this->getConnection();
      $response = $mailchimp->ping->get();
      return ($response->health_status == "Everything's Chimpy!");
    }
    catch (ClientException $e) {
      return FALSE;
    }
    catch (ConnectException $e) {
      return FALSE;
    }
  }

  /**
   * Test connection.
   *
   * @return bool
   *   True if connection successful.
   */
  public function getDefaultList() {
    $mailchimp = $this->getConnection();
    return $this->config->get('mailchimp_default_list');
  }

}
